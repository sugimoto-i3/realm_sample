//
//  AppDelegate.h
//  realm-test
//
//  Created by Itaru on 2016/02/22.
//  Copyright © 2016年 Itaru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

