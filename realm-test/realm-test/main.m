//
//  main.m
//  realm-test
//
//  Created by Itaru on 2016/02/22.
//  Copyright © 2016年 Itaru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
