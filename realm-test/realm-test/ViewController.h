//
//  ViewController.h
//  realm-test
//
//  Created by Itaru on 2016/02/22.
//  Copyright © 2016年 Itaru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel* timeLabel;
@property (strong, nonatomic) RLMNotificationToken* token;

@end

