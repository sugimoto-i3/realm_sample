//
//  ViewController.m
//  realm-test
//
//  Created by Itaru on 2016/02/22.
//  Copyright © 2016年 Itaru. All rights reserved.
//

#import "ViewController.h"
#import "realmIO.h"

#define RECORD_NUMBER 10000
#define DATE_FORMAT @"yyyy/MM/dd HH:mm:ss.SSS"

// インスタンス変数
NSDate *startDate;
NSDate *endDate;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    
    self.timeLabel.text = @"0秒";
    
    [super viewDidLoad];
    
}

- (IBAction)cleanData:(id)sender {
    
    self.timeLabel.text = @"0秒";
    
    @autoreleasepool {
        // all Realm usage here
        RLMRealm *realm = [RLMRealm defaultRealm];
        
        [realm beginWriteTransaction];
        [realm deleteAllObjects]; // 全件削除
        [realm commitWriteTransaction];
    }
    
    // Realmファイルを全件削除する場合下記の付随ファイルも削除
    NSFileManager *manager = [NSFileManager defaultManager];
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    NSArray<NSString *> *realmFilePaths = @[
                                            config.path,
                                            [config.path stringByAppendingPathExtension:@"lock"],
                                            [config.path stringByAppendingPathExtension:@"log_a"],
                                            [config.path stringByAppendingPathExtension:@"log_b"],
                                            [config.path stringByAppendingPathExtension:@"note"]
                                            ];
    for (NSString *path in realmFilePaths) {
        NSError *error = nil;
        [manager removeItemAtPath:path error:&error];
        if (error) {
            // handle error
        }
    }
    

}

- (IBAction)insertData:(id)sender {
    // 開始時間
    startDate = [NSDate date];
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    NSInteger max = RECORD_NUMBER;
    // トランザクション開始
    [realm beginWriteTransaction];
    
    for (NSInteger i = 0; i < max; i++) {
        realmIO *friend = [[realmIO alloc] init];
        friend.p_id = i;
        friend.name = @"Aisan Taro";
        friend.age  = @"28";
        // オブジェクト追加
        [realm addObject:friend];
    }
    
    // commit
    [realm commitWriteTransaction];
    
    // 処理終了位置で現在時間を代入
    endDate = [NSDate date];
    
    // 開始時間と終了時間の差を表示
    NSTimeInterval interval = [endDate timeIntervalSinceDate:startDate];
    NSLog(@"処理開始時間 = %@",[self getDateString:startDate]);
    NSLog(@"処理終了時間 = %@",[self getDateString:endDate]);
    NSLog(@"INSERT処理時間 = %.3f秒",interval);
    self.timeLabel.text = [NSString stringWithFormat:@"INSERT処理時間 = %.3f秒",interval];
}

- (IBAction)updataData:(id)sender {
    // 開始時間
    startDate = [NSDate date];
    RLMRealm *realm = [RLMRealm defaultRealm];
    NSInteger max = RECORD_NUMBER;
    // トランザクション開始
    [realm beginWriteTransaction];
    for (NSInteger i = 0; i < max; i++) {
        realmIO *friend = [[realmIO alloc] init];
        friend.p_id = i;
        friend.name = @"Aisan Jiro";
        friend.age  = @"27";
        // オブジェクト追加
        [realmIO createOrUpdateInRealm:realm withValue:friend];
    }
    

    [realm commitWriteTransaction];
    
    // 処理終了位置で現在時間を代入
    endDate = [NSDate date];
    
    // 開始時間と終了時間の差を表示
    NSTimeInterval interval = [endDate timeIntervalSinceDate:startDate];
    NSLog(@"処理開始時間 = %@",[self getDateString:startDate]);
    NSLog(@"処理終了時間 = %@",[self getDateString:endDate]);
    NSLog(@"INSERT処理時間 = %.3f秒",interval);
    self.timeLabel.text = [NSString stringWithFormat:@"UPDATA処理時間 = %.3f秒",interval];
}

- (IBAction)getData:(id)sender {
    // 開始時間
    startDate = [NSDate date];
    
    RLMResults *result = [realmIO allObjects];
    for (realmIO *friend in result) {
        //NSLog(@"id:%ld name:%@ age:%@",friend.p_id ,friend.name, friend.age);
        NSInteger p_id  = friend.p_id;
        NSString *key   = friend.name;
        NSString *value = friend.age;
    }
    
    // 処理終了位置で現在時間を代入
    endDate = [NSDate date];
    
    // 開始時間と終了時間の差を表示
    NSTimeInterval interval = [endDate timeIntervalSinceDate:startDate];
    NSLog(@"処理開始時間 = %@",[self getDateString:startDate]);
    NSLog(@"処理終了時間 = %@",[self getDateString:endDate]);
    NSLog(@"INSERT処理時間 = %.3f秒",interval);
    self.timeLabel.text = [NSString stringWithFormat:@"INSERT処理時間 = %.3f秒",interval];
}

/**
 *  日付をミリ秒までの表示にして文字列で返す
 *
 *  @param date データ
 *
 *  @return 時間
 */
- (NSString*)getDateString:(NSDate*)date
{
    // 日付フォーマットオブジェクトの生成
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    // フォーマットを指定の日付フォーマットに設定
    [dateFormatter setDateFormat:DATE_FORMAT];
    // 日付の文字列を生成
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
