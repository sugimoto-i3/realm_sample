//
//  realmIO.h
//  realm-test
//
//  Created by Itaru on 2016/02/22.
//  Copyright © 2016年 Itaru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface realmIO : RLMObject
@property NSInteger p_id;
@property NSString* name;
@property NSString* age;
@end